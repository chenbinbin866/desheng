import router from "@/router"
import { CodeToText, TextToCode } from 'element-china-area-data'
import { pcaa } from 'area-data'

// 判断是否为空 不为空返回数据 为空返回--
export const isNotNull = (data, str) => {
  if (isBlank(str)) {
    str = "--"
  }
  if (data == null || data === 'null' || data === '' || data === undefined || data === 'undefined' || data === 'unknown') {
    return str
  } else {
    return data
  }
}

// 判断是否为空 返回true/false
export const isBlank = (data) => {
  if (data == null || data === 'null' || data === '' || data === undefined || data === 'undefined' || data === 'unknown' || !data || !/[^\s]/.test(data)) {
    return true
  } else {
    return false
  }
}

// 返回上一页
export const backPage = () => {
  router.go(-1)
}

const address = {
    CodeToText:function (data) {
        if(isBlank(data)) return [];
        let old_data = data,new_data = []
        old_data.forEach((code)=>{
            new_data.push(CodeToText[code])
        })
        return new_data;
    },
    TextToCode:function (data) {
        if(isBlank(data)) return [];
        let old_data = data,new_data = []
        old_data.forEach((item,index)=>{
            if(index==0) new_data.push(TextToCode[item].code)
            if(index==1) new_data.push(TextToCode[old_data[index-1]][item].code)
            if(index==2) new_data.push(TextToCode[old_data[index-2]][old_data[index-1]][item].code)
            //if(index==3) new_data.push(TextToCode[old_data[index-3]][old_data[index-2]][old_data[index-1]][item].code)
        })
        return new_data;
    }
}

const utils = { isBlank, isNotNull, backPage, address }
export default utils
