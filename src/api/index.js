import request from './request'


export async function get(url, data = "", options = {}) {
  if (data) {
    for (const datasKey in data) {
      if (void(0) != data[datasKey]) {
        url += "&" + datasKey + "=" + data[datasKey];
      }
    }
  }

  return await request({
    url: url,
    method: "get",
    data: data,
    options: options,
  });
}

export async function post(url, data = "", options = {}) {
  // let datas = {};
  // if (data.data) {
  //   datas = data.data;
  // }
  return request({
    url: url,
    method: "post",
    data: data,
    options: options,
  });
}

export async function message(options) {}
export function getInfo(accessToken) {
  // console.log("检查登录1");
  return request({
    url: "user",
    method: "post",
    data: {
      accessToken,
    },
  });
}

export function logout() {
  return request({
    url: "/logout",
    method: "post",
  });
}
const ske = {
  get: get,
  post: post,
};
export default ske;
