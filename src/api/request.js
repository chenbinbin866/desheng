/**
 *   @description 接口请求拦截
 *   @author Xiaohui Zeng
 *   @date 2020/5/14
 */
import axios from 'axios'
import {
    themeData
} from "@/config/config";
import  router from  '@/router'

const {
    baseURL,
    contentType,
    debounce,
    invalidCode,
    noPermissionCode,
    requestTimeout,
    successCode,
    tokenName,
    loginInterception
} = themeData;

import {Message} from 'view-design';
// 创建一个axios实例
let url;
if (process.env.NODE_ENV === "development") {
    //alert("开发环境");
    //url='http://desheng.gudemai.cn/app/huotuan.php?r=';
    url='http://desheng.alitakego.com/app/huotuan.php?r=';
}else {
    //url='http://desheng.alitakego.com/app/huotuan.php?r=';
    //alert("生产环境");
    url = window.location.protocol + "//" + window.location.host+'/app/huotuan.php?r=';
}
//url='http://local.huotuan.com/app/huotuan.php?r=';
const service = axios.create({
    baseURL: url, // url = base url + request url
    timeout: 30000, // 请求超时时间 默认30秒
    headers: {
        'Content-Type': 'application/json'
    }
})

import {isArray} from "@/utils/validate";

// 请求拦截器
service.interceptors.request.use(config => {
        const token = localStorage.getItem('userToken');

        // 判断是否存在token，如果存在的话，则每个http header都加上token
      //  console.log(token);
        if (token) {
            config.headers['accessToken'] = token
        }

        return config
    },
    error => {
        // debug
        //console.log(error)
        return Promise.reject(error)
    }
)

// 响应拦截器
service.interceptors.response.use(
    response => {
        console.log('response',response)
        const {status1, data, config} = response;
        /*if (!data) {
            errorMsg(`后端接口异常`);
            return datas;
        }*/

        //var tmp_data = JSON.stringify(data)
        //tmp_data = JSON.parse(tmp_data)
        var tmp_data
        if (typeof response.data == "object") {
            tmp_data = response.data;
        } else {
            tmp_data = JSON.parse(response.data);
            if (typeof tmp_data == "object") {

            } else {
                tmp_data = JSON.parse(tmp_data);
            }
        }

        //console.log('data',data)
        //console.log('tmp_data',tmp_data)

        const {status} = tmp_data;
        const datas = tmp_data.result;

        //console.log('datas',datas)

        const codeVerificationArray = isArray(successCode)
            ? [...successCode]
            : [...[successCode]];

        if (codeVerificationArray.includes(status)) {

            if (status == 2) {
                Message.success(datas.message || `操作成功`, "success");
            }
            return datas;
        } else {
            let msg = datas.message;

            handleCode(status, msg);
            return Promise.reject(
                JSON.stringify({url: config.url, status, msg}) || "Error"
            );
        }

    },
    error => {
        // debug

        Message.error(res.msg || 'Error')
        return Promise.reject(error)
    }
)

const handleCode = (status, msg) => {

    switch (status) {
        case invalidCode:
            Message.warning(msg || `后端接口${status}异常`, "error");
            store.dispatch("user/resetAccessToken").catch(() => {
            });
            if (loginInterception) {
                location.reload();
            }
            break;
        case noPermissionCode:
            router.push({path: "/401"}).catch(() => {
            });
            break;
        case -1:
         //   console.log(router);
            router.push({path: msg}).catch(() => {
            });
            break;
        default:
            Message.warning(msg || `后端接口${status}异常`, "error");
            break;
    }
};

export default service
