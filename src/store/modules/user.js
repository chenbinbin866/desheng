import $http from '@/api/index'
const state = {
    // 用户信息数据
    userData: localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')) : {},
    // 是否登陆
    isLogin: localStorage.getItem('userData') ? true : false,
    // 角色权限集合
    roles: []
}

const mutations = {
    // 设置角色
    setRoles: (state, roles) => {
        state.roles = roles
    },
    // 设置用户数据
    setUserData(state, data) {
        state.userData = data
    },
    // 修改登陆状态
    setIsLogin(state, data) {
        state.isLogin = data
    }
}

const actions = {
    // 登录
    login({commit}, userInfo) {
        console.log(12312321)
        const {userName, passWord} = userInfo

        return new Promise((resolve, reject) => {
            $http.post("login", {userName: userName, passWord: passWord}).then((resp) => {

                console.log(resp)
                localStorage.setItem('userData', JSON.stringify(resp.member))
                localStorage.setItem('userToken', resp.token)
                commit('setUserData', resp.member)
                // 设置登录状态为false
                commit('setIsLogin', true)

                resolve(resp)


            });
        }).catch(error => {
            reject(error)
        })

    },

    // 获取用户信息
    getInfo({ commit, state }) {
        return new Promise((resolve, reject) => {
            if (!localStorage.getItem('userData')) {
                reject('验证失败，请重新登录')
            }
            const userInfo = JSON.parse(localStorage.getItem('userData'))
            userInfo.roles = ['admin'];
            //this.login(userInfo);
            if(!userInfo.roles) {
                console.error('权限不能为空数组，否则路由死循环')
                return
            }

            commit('setRoles', userInfo.roles)
            resolve(userInfo)
        })
    },

    // 退出登录
    logout({ commit, state, dispatch }) {
        return new Promise((resolve, reject) => {
            // commit('setToken', '')
            // 清除用户数据
            commit('setUserData', '')
            // 清楚用户权限集合
            commit('setRoles', [])
            // 设置登录状态为false
            commit('setIsLogin', false)
            // 清除缓存的用户数据
            localStorage.removeItem('userData')
            localStorage.removeItem('userToken')
            // 清除缓存的多页签数据
            dispatch('tagsView/setRouterArr', [], { root: true })
            localStorage.removeItem('dataRouter')
            resolve()
        })
    },

    // 设置用户数据
    setUserData(context, data) {
        context.commit('setUserData', data);
    },
    // 修改登陆状态
    setIsLogin(context, data) {
        context.commit('setIsLogin', data);
    }



}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
