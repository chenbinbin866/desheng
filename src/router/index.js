import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
import Main from "@/components/main/main";
import webMain from "@/components/webmain/webmain";
import merchantMain from "@/components/public/merchant/main";

/**
 meta: {
    hide: false, 是否在左侧菜单显示 不显示请设为 true
    title: "列表页面", 菜单标题
    icon: "md-grid", 图标
    roleId: 1 菜单权限id 不填写等同于不需要权限校验
    singlePage: true 单页 非嵌套栏目显示
}
 */

// 不需要权限校验的静态路由
export const constantRoutes = [
    {
        path: "/",
        name: "/",
        redirect: '/pc/index',
        component: Main,
        meta: {
            hide: false,
            title: "Dashboard",
            icon: "md-speedometer",
        },
        children: [
            {
                path: "/web/login",
                name: "web_login",
                meta: {
                    hide: false,
                    title: "商户登录"
                },
                component: () => import("@/views/web/login/login")
            },
        ]
    },
    {
        path: "/login",
        name: "login",
        meta: {
            hide: true,
            title: "登录"
        },
        component: () => import("@/views/login/login")
    },

    {
        path: "/web",
        name: "Web",
        component: webMain,
        meta: {
            hide: true,
            title: "首页",
        },
        children: [

            {
                path: "/web/index",
                name: "web_index",
                meta: {
                    hide: false,
                    title: "工作台",
                    permission: ['admin']
                },
                component: () => import("@/views/web/index/index")
            },
            {
                path: "/web/seller/sellerinfo",
                name: "Sellerinfo",
                meta: {
                    hide: false,
                    title: "店铺基本信息管理",
                    permission: ['admin']
                },
                component: () => import("@/views/web/seller/sellerinfo/sellerinfo")

            },
            {

                path: "/web/seller/vendershop",
                name: "vendershop",
                meta: {
                    hide: false,
                    title: "店铺基本设置",
                    permission: ['admin']
                },
                component: () => import("@/views/web/seller/vendershop/vendershop")

            },{

                path: "/web/seller/venderBrand",
                name: "venderBrand",
                meta: {
                    hide: false,
                    title: "品牌管理",
                    permission: ['admin']
                },
                component: () => import("@/views/web/seller/venderBrand/venderBrand")

            },
            {

                path: "/web/seller/add",
                name: "seller_add",
                meta: {
                    hide: false,
                    title: "地址管理",
                    permission: ['admin']
                },
                component: () => import("@/views/web/seller/add/add")

            },
            {
                path: "/web/seller/service",
                name: "seller_service",
                meta: {
                    hide: false,
                    title: "商家服务中心",
                    permission: ['admin']
                },
                component: () => import("@/views/web/seller/service")

            },
            {
                path: "/web/goods/manage/navigation",
                name: "web_goods_manage_navigation",
                meta: {
                    hide: false,
                    hide_menu: true,
                    title: "选择分类",
                    permission: ['admin']
                },
                component: () => import("@/views/web/goods/manage/navigation")

            },
            {
                path: "/web/goods/manage/edit",
                name: "web_goods_manage_edit",
                meta: {
                    hide: false,
                    hide_menu: true,
                    title: "添加商品",
                    permission: ['admin']
                },
                component: () => import("@/views/web/goods/manage/edit")

            },
            {
                path: "/web/goods/list",
                name: "web_goods_list",
                meta: {
                    hide: false,
                    hide_menu: false,
                    title: "商品列表",
                    permission: ['admin']
                },
                component: () => import("@/views/web/goods/list")

            },
            {
                path: "/web/goods/recycle",
                name: "web_goods_recycle",
                meta: {
                    hide: false,
                    hide_menu: false,
                    title: "回收站",
                    permission: ['admin']
                },
                component: () => import("@/views/web/goods/recycle")

            },
            {
                path: "/web/goods/request",
                name: "web_goods_request",
                meta: {
                    hide: false,
                    hide_menu: false,
                    title: "商品问答",
                    permission: ['admin']
                },
                component: () => import("@/views/web/goods/request")

            },
            {
                path: "/web/goods/stock",
                name: "web_goods_stock",
                meta: {
                    hide: false,
                    hide_menu: false,
                    title: "分区库存管理",
                    permission: ['admin']
                },
                component: () => import("@/views/web/goods/stock")

            },
            {
                path: "/web/goods/attr",
                name: "web_goods_attr",
                meta: {
                    hide: false,
                    hide_menu: false,
                    title: "属性管理",
                    permission: ['admin']
                },
                component: () => import("@/views/web/goods/attr")

            },
            {
                path: "/web/goods/attr/manage",
                name: "web_goods_attr_manage",
                meta: {
                    hide: false,
                    hide_menu: false,
                    title: "类目属性管理",
                    permission: ['admin']
                },
                component: () => import("@/views/web/goods/attr/manage")

            },
            {
                path: "/web/order/list",
                name: "web_order_list",
                meta: {
                    hide: false,
                    hide_menu: false,
                    title: "订单管理",
                    permission: ['admin']
                },
                component: () => import("@/views/web/order/list/list")

            },
            {
                path: "/web/order/detail",
                name: "web_order_detail",
                meta: {
                    hide: false,
                    hide_menu: true,
                    hide_nav: true,
                    title: "订单详情",
                    permission: ['admin']
                },
                component: () => import("@/views/web/order/detail/list")

            },
            {
                path: "/web/order/chuku",
                name: "web_order_chuku",
                meta: {
                    hide: false,
                    hide_menu: false,
                    hide_nav: false,
                    title: "",
                    permission: ['admin']
                },
                component: () => import("@/views/web/order/chuku/index")

            },

            {
                path: "/web/order/preOrder",
                name: "web_order_preOrder",
                meta: {
                    hide: false,
                    hide_menu: false,
                    hide_nav: false,
                    title: "",
                    permission: ['admin']
                },
                component: () => import("@/views/web/order/preOrder/list")

            },
            {
                path: "/web/order/cuiDanList",
                name: "web_order_cuiDanList",
                meta: {
                    hide: false,
                    hide_menu: false,
                    hide_nav: false,
                    title: "",
                    permission: ['admin']
                },
                component: () => import("@/views/web/order/cuiDanList/list")

            },
            {
                path: "/web/order/abnormal",
                name: "web_order_abnormal",
                meta: {
                    hide: false,
                    hide_menu: false,
                    hide_nav: false,
                    title: "",
                    permission: ['admin']
                },
                component: () => import("@/views/web/order/abnormal/list")

            },
            {
                path: "/web/order/initJmiShopOrderList",
                name: "web_order_initJmiShopOrderList",
                meta: {
                    hide: false,
                    hide_menu: false,
                    hide_nav: false,
                    title: "",
                    permission: ['admin']
                },
                component: () => import("@/views/web/order/initJmiShopOrderList/list")

            },
            {
                path: "/web/order/delayDeliveryReport",
                name: "web_order_delayDeliveryReport",
                meta: {
                    hide: false,
                    hide_menu: false,
                    hide_nav: false,
                    title: "",
                    permission: ['admin']
                },
                component: () => import("@/views/web/order/delayDeliveryReport/list")

            }
            ,{
                path: "/web/order/exportCenter",
                name: "web_order_exportCenter",
                meta: {
                    hide: false,
                    hide_menu: false,
                    hide_nav: false,
                    title: "",
                    permission: ['admin']
                },
                component: () => import("@/views/web/order/exportCenter/list")

            },
            {
                path: "/web/order/outOrderPrint",
                name: "web_order_outOrderPrint",
                meta: {
                    hide: false,
                    hide_menu: false,
                    hide_nav: false,
                    title: "",
                    permission: ['admin']
                },
                component: () => import("@/views/web/order/outOrderPrint/list")

            },
            {
                path: "/web/peisong/wuliu/index",
                name: "web_peisong_wuliu_index",
                meta: {
                    hide: false,
                    hide_menu: false,
                    hide_nav: false,
                    title: "",
                    permission: ['admin']
                },
                component: () => import("@/views/web/peisong/wuliu/index")
            },
            {
                path: "/web/shouhou/self/index",
                name: "web_shouhou_self_index",
                meta: {
                    hide: false,
                    hide_menu: false,
                    hide_nav: false,
                    title: "",
                    permission: ['admin']
                },
                component: () => import("@/views/web/shouhou/self/index")

            },
            {
                path: "/web/storage/stock",
                name: "web_storage_stock",
                meta: {
                    hide: false,
                    hide_menu: false,
                    title: "分区仓库管理",
                    permission: ['admin']
                },
                component: () => import("@/views/web/storage/stock")

            },
            {
                path: "/web/peisong/facesheet",
                name: "web_peisong_facesheet",
                meta: {
                    hide: false,
                    hide_menu: false,
                    title: "无界电子面单服务",
                    permission: ['admin']
                },
                component: () => import("@/views/web/peisong/facesheet")

            },
        ]


    },
    {
        path: "/merchant",
        name: "merchant",
        component: merchantMain,
        meta: {
            hide: false,
            title: "商户",
            icon: "md-heart-outline",
            singlePage: true
        },
        children: [
            {
                path: "/merchant/apply",
                name: "merchant_apply",
                meta: {
                    hide: false,
                    title: "商户入驻",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/merchant/apply")
            },
            {
                path: "/merchant/apply/index",
                name: "merchant_apply_index",
                meta: {
                    hide: false,
                    title: "商户入驻",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/merchant/apply/index")
            },
            {
                path: "/merchant/apply/form",
                name: "merchant_apply_form",
                meta: {
                    hide: false,
                    title: "商户入驻",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/merchant/apply/form")
            },
            {
                path: "/merchant/apply/success",
                name: "merchant_apply_success",
                meta: {
                    hide: false,
                    title: "入驻成功",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/merchant/apply/success")
            },
        ]
    },
    {
        path: "/pc",
        name: "pc",
        component: Main,
        meta: {
            hide: false,
            title: "前台",
            icon: "md-heart-outline",
            singlePage: true
        },
        children: [
            {
                path: "/pc/login",
                name: "pc_login",
                meta: {
                    hide: false,
                    title: "登录",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/login/login")
            },
            {
                path: "/pc/goods/item",
                name: "pc_goods_item",
                meta: {
                    hide: false,
                    title: "商品详情",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/goods/item")
            },{
                path: "/pc/addToCart",
                name: "pc_addToCart",
                meta: {
                    hide: false,
                    title: "商品详情",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/goods/addToCart/index")
            },
            {
                path: "/pc/regist",
                name: "pc_regist",
                meta: {
                    hide: false,
                    title: "注册",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/regist/regist")
            },
            {
                path: "/pc/regist_success",
                name: "pc_regist_success",
                meta: {
                    hide: false,
                    title: "注册成功",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/regist/success")
            },
            {
                path: "/pc/home",
                name: "pc_home",
                meta: {
                    hide: false,
                    title: "我的京东",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/home/home")
            },
            {
                path: "/pc/order",
                name: "pc_order",
                meta: {
                    hide: false,
                    title: "我的订单",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/order/list/list")
            },
            {
                path: "/pc/order/detail",
                name: "pc_order_detail",
                meta: {
                    hide: false,
                    title: "订单详情",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/order/detail/detail")
            },
            {
                path: "/pc/member/active",
                name: "pc_member_active",
                meta: {
                    hide: false,
                    title: "我的活动",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/order/active/list")
            },
            {
                path: "/pc/order/scope",
                name: "pc_order_scope",
                meta: {
                    hide: false,
                    title: "我的评价",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/order/scope/scope")
            },
            {
                path: "/pc/scope/list",
                name: "pc_scope_list",
                meta: {
                    hide: false,
                    title: "评价晒单",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/order/scope/list")
            },
            {
                path: "/pc/order/cgou",
                name: "pc_order_cgou",
                meta: {
                    hide: false,
                    title: "我的常购商品",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/order/cgou/list")
            },
            {
                path: "/pc/order/touchsub",
                name: "pc_order_touchsub",
                meta: {
                    hide: false,
                    title: "代下单",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/order/touchsub/list")
            },
            {
                path: "/pc/order/gzgoods",
                name: "pc_order_gzgoods",
                meta: {
                    hide: false,
                    title: "关注的商品",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/order/gzgoods/list")
            },
            {
                path: "/pc/order/gzstore",
                name: "pc_order_gzstore",
                meta: {
                    hide: false,
                    title: "关注的店铺",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/order/gzstore/list")
            },
            {
                path: "/pc/order/gzzj",
                name: "pc_order_gzzj",
                meta: {
                    hide: false,
                    title: "关注的专辑",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/order/gzzj/list")
            },
            {
                path: "/pc/order/sznr",
                name: "pc_order_sznr",
                meta: {
                    hide: false,
                    title: "收藏的内容",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/order/sznr/list")
            },
            {
                path: "/pc/order/gzhd",
                name: "pc_order_gzhd",
                meta: {
                    hide: false,
                    title: "关注的活动",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/order/gzhd/list")
            },
            {
                path: "/pc/user/info",
                name: "pc_user_info",
                meta: {
                    hide: false,
                    title: "账户信息",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/user/info/info")
            },
            {
                path: "/pc/user/showimg",
                name: "pc_user_showimg",
                meta: {
                    hide: false,
                    title: "账户信息",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/user/info/showimg")
            },
            {
                path: "/pc/user/more",
                name: "pc_user_more",
                meta: {
                    hide: false,
                    title: "账户信息",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/user/info/more")
            },
            {
                path: "/pc/user/address",
                name: "pc_user_address",
                meta: {
                    hide: false,
                    title: "收货地址",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/user/info/address")
            },
            {
                path: "/pc/user/quan",
                name: "pc_user_quan",
                meta: {
                    hide: false,
                    title: "优惠券",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/user/quan/quan")
            },
            {
                path: "/pc/index",
                name: "pc_index",
                meta: {
                    hide: false,
                    title: "京东",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/index/index")
            },
            {
                path: "/pc/fen",
                name: "pc_fen",
                meta: {
                    hide: false,
                    title: "京东",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/index/Fen")
            },
            ,
            {
                path: "/pc/lead",
                name: "pc_lead",
                meta: {
                    hide: false,
                    title: "优惠券",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/index/lead")
            },
            {
                path: "/pc/seckil",
                name: "pc_seckil",
                meta: {
                    hide: false,
                    title: "秒杀",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/index/seckil")
            },
            {
                path: "/pc/specail",
                name: "pc_specail",
                meta: {
                    hide: false,
                    title: "特价",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/index/specail")
            },
            {
                path: "/pc/index/createorder",
                name: "pc_index_createorder",
                meta: {
                    hide: false,
                    title: "京东",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/index/createorder")
            },
            {
                path: "/pc/index/cart",
                name: "pc_index_cart",
                meta: {
                    hide: false,
                    title: "我的购物车",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/index/cart")
            },
            {
                path: "/pc/index/cashier",
                name: "pc_index_cashier",
                meta: {
                    hide: false,
                    title: "收银台",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/index/cashier")
            },
            {
                path: "/pc/index/pay_success",
                name: "pc_index_pay_success",
                meta: {
                    hide: false,
                    title: "支付结果",
                    icon: 'md-heart-outline'
                },
                component: () => import("@/views/pc/index/pay_success")
            },
        ]
    },
]
// 需要权限校验的异步路由
export const asyncRoutes = [

    {
        path: "/form",
        name: "form",
        component: Main,
        meta: {
            hide: false,
            title: "表单页面",
            icon: "md-cube",
            roleId: 1
        },
        children: [
            {
                path: "/form/basic_form",
                name: "basic-form",
                meta: {
                    hide: false,
                    title: "基础表单",
                    roleId: 2
                },
                component: () => import("@/views/form/basic_form")
            },
            {
                path: "/form/advanced_form",
                name: "advanced_form",
                meta: {
                    hide: false,
                    title: "高级表单",
                    roleId: 3
                },
                component: () => import("@/views/form/advanced_form")
            },
        ]
    },
    {
        path: "/list",
        name: "list",
        component: Main,
        meta: {
            hide: false,
            title: "列表页面",
            icon: "md-grid",
            roleId: 4
        },
        children: [
            {
                path: "/list/user_list",
                name: "user_list",
                meta: {
                    hide: false,
                    title: "用户列表",
                    roleId: 6
                },
                component: () => import("@/views/list/user_list")
            },
            {
                path: "/list/goods_list",
                name: "goods_list",
                meta: {
                    hide: false,
                    title: "商品列表",
                    roleId: 7
                },
                component: () => import("@/views/list/goods_list")
            }
        ]
    },
    {
        path: "/search",
        name: "search",
        component: Main,
        meta: {
            hide: false,
            title: "搜索页面",
            icon: "md-search",
        },
        children: [
            {
                path: "/search/search_article",
                name: "search_article",
                meta: {
                    hide: false,
                    title: "搜索列表（文章）",
                },
                component: () => import("@/views/search/search_article")
            },
            {
                path: "/search/search_projects",
                name: "search_projects",
                meta: {
                    hide: false,
                    title: "搜索列表（项目）",
                },
                component: () => import("@/views/search/search_projects")
            }
        ]
    },
    {
        path: "/detail",
        name: "detail",
        component: Main,
        meta: {
            hide: false,
            title: "详情页面",
            icon: "md-list-box",
            roleId: 11
        },
        children: [
            {
                path: "/detail/basic_detail",
                name: "basic_detail",
                meta: {
                    hide: false,
                    title: "基础详情",
                    roleId: 12
                },
                component: () => import("@/views/detail/basic_detail")
            },
            {
                path: "/detail/advanced_detail",
                name: "advanced_detail",
                meta: {
                    hide: false,
                    title: "高级详情",
                    roleId: 13
                },
                component: () => import("@/views/detail/advanced_detail")
            }
        ]
    },
    {
        path: "/result",
        name: "result",
        component: Main,
        meta: {
            hide: false,
            title: "结果页面",
            icon: "md-checkmark-circle-outline",
            roleId: 14
        },
        children: [
            {
                path: "/result/result_success",
                name: "result_success",
                meta: {
                    hide: false,
                    title: "成功页面",
                    roleId: 15
                },
                component: () => import("@/views/result/result_success")
            },
            {
                path: "/result/result_fail",
                name: "result_fail",
                meta: {
                    hide: false,
                    title: "失败页面",
                    roleId: 16
                },
                component: () => import("@/views/result/result_fail")
            }
        ]
    },
    {
        path: "/setting",
        name: "setting",
        component: Main,
        meta: {
            hide: false,
            title: "设置页面",
            icon: "md-options",
        },
        children: [
            {
                path: "/setting/setting_user",
                name: "setting_user",
                meta: {
                    hide: false,
                    title: "个人中心",
                },
                component: () => import("@/views/setting/setting_user")
            },
            {
                path: "/setting/setting_account",
                name: "setting_account",
                meta: {
                    hide: false,
                    title: "个人设置",
                },
                component: () => import("@/views/setting/setting_account")
            }
        ]
    },
    {
        path: "/editor",
        name: "editor",
        component: Main,
        meta: {
            hide: false,
            title: "编辑器",
            icon: "ios-create-outline",
        },
        children: [
            {
                path: "/editor/wangEditor",
                name: "editor_wangEditor",
                meta: {
                    hide: false,
                    title: "wangEditor",
                    icon: 'ios-create-outline'
                },
                component: () => import("@/views/editor/wangEditor")
            }
        ]
    },
    {
        path: "/city",
        name: "city",
        component: Main,
        meta: {
            hide: false,
            title: "高级组件",
            icon: "ios-paper-plane-outline",
        },
        children: [
            {
                path: "/city/city",
                name: "city_city",
                meta: {
                    hide: false,
                    title: "城市选择",
                    icon: 'ios-create-outline'
                },
                component: () => import("@/views/city/city")
            }
        ]
    }
]

const createRouter = () => new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({y: 0}),
    routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
}

// 解决跳转同一个路由报错
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

export default router
