/**
 *   @description 配置文件
 *   @author Xiaohui Zeng
 *   @date 2019/10/5
 */
// 默认系统名称
export const siteTitle = '京东'
// 默认首页
export const indexPage = 'pc_index'
// 默认登录页
export const loginPage = 'pc_login'
//export const upload_url = 'http://desheng.alitakego.com.cn/app/huotuan.php?r=/util/uploader/upload'
//export const loginPage = 'web_login'
//export const upload_url = 'http://desheng.alitakego.com/app/huotuan.php?r=/util/uploader/upload'

let up_url;
if (process.env.NODE_ENV === "development") {
    //alert("开发环境");
    //url='http://desheng.gudemai.cn/app/huotuan.php?r=';
    up_url='http://desheng.alitakego.com/app/huotuan.php?r=/util/uploader/upload';
}else {
    //url='http://desheng.alitakego.com/app/huotuan.php?r=';
    //alert("生产环境");
    up_url = window.location.protocol + "//" + window.location.host+'/app/huotuan.php?r=/util/uploader/upload';
}

export const upload_url = up_url
export const currentupload = '' //当前上传

// 主题风格
export const themeData = {
    themeType: 'dark', // 主题风格配置 dark(经典酷黑) 或者 light(极简雅白) 默认dark经典酷黑
    isTabsShow: false, // 是否显示多页签 默认true
    headMaxWidth: false, // 栏目头部是否通顶（最大宽度） 默认false
    successCode: [1, 2],
    //登录失效code
    invalidCode: 402,
    //无权限code
    noPermissionCode: 401,
}

