import Vue from "vue";
import App from "./App.vue";
import router from "./router"; // 路由
import './router/permission'; // 加载路由权限控制
import store from "./store";
import 'babel-polyfill' // 解决ie兼容问题
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
import "./css/public.less"; // 公共css样式
Vue.use(ViewUI);


import { pcaa } from 'area-data-vue';
import AreaLinkageVue from 'area-linkage-vue';
Vue.prototype.$pcaa = pcaa;
Vue.use(AreaLinkageVue)

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI,{ size: 'mini' });//, { size: 'mini' }

import {upload_url} from "@/config/config";
import {currentupload} from "@/config/config";

//import ElSelectArea from '@/lenyue/el-select-area'
//Vue.component('el-select-area',ElSelectArea)

// 工具
import {backPage, isNotNull, isBlank} from "./utils/util";
import utils from "./utils/util";
Vue.prototype.$utils = utils


// 按钮组件级别权限校验
import checkPermission from "./utils/permission";
// 配置文件
import {indexPage, loginPage} from "./config/config";


import $http from "@/api/index";
import $a from "@/api/a";
Vue.prototype.$http = $http;
Vue.prototype.$a = $a;
Vue.prototype.upload_url = upload_url;
import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)
// 引入mock文件 目前登录使用的是在线mock
// require('./mock/index') // mock 方式，正式发布时，注释掉该处即可
import VueKindEditor from 'vue-kindeditor'
import 'kindeditor/kindeditor-all-min.js'
import 'kindeditor/themes/default/default.css'
Vue.use(VueKindEditor)

Vue.prototype.$backPage = backPage
Vue.prototype.$isNotNull = isNotNull
Vue.prototype.$isBlank = isBlank
Vue.prototype.$indexPage = indexPage
Vue.prototype.$loginPage = loginPage
Vue.prototype.$checkPermission = checkPermission

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
