vue-area-linkage area-data
npm i --save vue-area-linkage
npm i --save area-data

import { pca, pcaa } from 'area-data';
import 'vue-area-linkage/dist/index.css';
import VueAreaLinkage from 'vue-area-linkage';
Vue.use(VueAreaLinkage)

// v5之前的版本
<area-select v-model="selected"></area-select>
<area-cascader v-model="selected2"></area-cascader>

// v5及之后的版本
<area-select v-model="selected" :data="pca"></area-select> // 省市
// 省市区：<area-select v-model="selected" :data="pcaa"></area-select>
<area-cascader v-model="selected2" :data="pca"></area-cascader> // 省市
// 省市区：<area-cascader v-model="selected2" :data="pcaa"></area-cascader>

//setting
<area-select type='all' :level='2' v-model="selected" :data="pcaa"></area-select>
<area-cascader type='all' v-model="selected2" :level='1' :data="pcaa"></area-cascader>

//setting
<area-select type='all' :level='2' v-model="selected"></area-select>
<area-cascader type='all' v-model="selected2" :level='2'></area-cascader>

    
maptree
vue和elementUI的一个三级联动地址 没有香港 澳门


el-select-area
npm i --save @femessage/el-select-area
import ElSelectArea from '@femessage/el-select-area'
components  ElSelectArea
<el-select-area v-model="area" />